# Changelog

All notable changes to this project will be documented in this file.

## [1.3.1]
### Added
* Add `.gitattributes` & `.gitignore`.

### Changed
* Change from `<config>` to `<base>`.

## [1.3.0]
### Changed
* Change from `<base>` to `<config>`.
* Use data attributes for `<config>`.

## [1.2.0]
### Added
* Support configure api url in `<base>`.

## [1.0.1]
### Changed
* Image has a fixed width instead of dynamic width.
* Default post per page change from 42 to 100

[1.3.1]: https://gitlab.com/joshua-avalon/szurubooru/client/compare/1.3.0...1.3.1
[1.3.0]: https://gitlab.com/joshua-avalon/szurubooru/client/compare/1.2.0...1.3.0
[1.2.0]: https://gitlab.com/joshua-avalon/szurubooru/client/compare/1.0.1...1.2.0
[1.0.1]: https://gitlab.com/joshua-avalon/szurubooru/client/compare/1.0.0...1.0.1
