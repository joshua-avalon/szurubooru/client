# Szurubooru Client

This is a modified client of Szurubooru.

To know more about Szurubooru,  please see the [original][szurubooru] or the [upstream repository][upstream].

## Usage

You should not use this directly unless you know what you are doing.

You can either download from release or [build it yourself](#build).

If you download from release, you should replace `__API_URL__` with you api path. The client is build to host on **root** of a domain.  If you want to host on a subdirectory, you need to build it youself.

## Build

```
npm ci
BASE_URL="__BASE_URL__" API_URL="__API_URL__" node build.js
```

Replace `__BASE_URL__` and `__API_URL__` with your only value.

[szurubooru]: https://github.com/rr-/szurubooru
[upstream]: https://gitlab.com/joshua-avalon/szurubooru/upstream
[docker]: https://gitlab.com/joshua-avalon/szurubooru/docker
